#pragma once

#include <wls/protocol/authority.hpp>
#include <wls/protocol/wls_operations.hpp>

#include <wls/chain/wls_object_types.hpp>
#include <wls/chain/witness_objects.hpp>

#include <boost/multi_index/composite_key.hpp>

namespace wls {
namespace chain {

using wls::protocol::htlc_hash_type;
using wls::protocol::htlc_ref_id_type;

class htlc_object : public object<htlc_object_type, htlc_object> {
  htlc_object() = delete;

 public:
  template <typename Constructor, typename Allocator>
  htlc_object(Constructor&& c, allocator<Allocator> a) {
    c(*this);
  }

  id_type id;

  htlc_ref_id_type refid; // ripemd160(from,to,preimage_hash)

  account_name_type from;
  account_name_type to;
  asset amount = asset(0, WLS_SYMBOL);
  asset reward = asset(0, WLS_SYMBOL); // optional, for rewarding 3rt party proving service.

  htlc_hash_type preimage_hash;
  uint16_t preimage_size;
  time_point_sec expiration;
};

// clang-format off
struct by_refid;
struct by_from_refid;
struct by_to_refid;
struct by_expiration;

typedef multi_index_container<
  htlc_object,
  indexed_by<
     ordered_unique< tag< by_id >, member< htlc_object, htlc_id_type, &htlc_object::id > >,
     ordered_unique< tag< by_refid >, member< htlc_object, htlc_ref_id_type, &htlc_object::refid > >,
     ordered_unique< tag< by_from_refid >,
        composite_key< htlc_object,
           member< htlc_object, account_name_type, &htlc_object::from >,
           member< htlc_object, htlc_ref_id_type, &htlc_object::refid >
        >,
        composite_key_compare< std::less< account_name_type >, std::less< htlc_ref_id_type > >
     >,
     ordered_unique< tag< by_to_refid >,
        composite_key< htlc_object,
           member< htlc_object, account_name_type, &htlc_object::to >,
           member< htlc_object, htlc_ref_id_type, &htlc_object::refid >
        >,
        composite_key_compare< std::less< account_name_type >, std::less< htlc_ref_id_type > >
     >,
     ordered_non_unique< tag< by_expiration >, member<htlc_object, time_point_sec, &htlc_object::expiration > >
  >,
  allocator< htlc_object >
> htlc_index;
// clang-format on

}  // namespace chain
}  // namespace wls

FC_REFLECT(wls::chain::htlc_object, (id)(refid)(from)(to)(amount)(reward)(preimage_hash)(preimage_size)(expiration))
CHAINBASE_SET_INDEX_TYPE(wls::chain::htlc_object, wls::chain::htlc_index)