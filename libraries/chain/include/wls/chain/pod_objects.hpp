#pragma once

#include <wls/protocol/authority.hpp>
#include <wls/protocol/wls_operations.hpp>

#include <wls/chain/wls_object_types.hpp>
#include <wls/chain/witness_objects.hpp>

#include <boost/multi_index/composite_key.hpp>

namespace wls {
namespace chain {

using pod_name_type = account_name_type;

/**
 * pod_object is split from account_object to make it easier to update later
 */
class pod_object : public object<pod_object_type, pod_object> {
  pod_object() = delete;

 public:
  template <typename Constructor, typename Allocator>
  pod_object(Constructor&& c, allocator<Allocator> a) : json_metadata(a) {
    c(*this);
  };

  id_type id;

  pod_name_type name;                /// pod name
  time_point_sec created;            /// when this pod is created
  asset fee = asset(0, WLS_SYMBOL);  /// fee to join this pod, default is zero (0 WLS)
  shared_string json_metadata;       /// anything else
  bool allow_join = true;            /// open for member to make a request to join
  uint16_t member_count = 0;         /// tracking total members in this pod, max 10k only
};

// clang-format off
struct by_name;
typedef multi_index_container<
    pod_object,
    indexed_by<
        ordered_unique< tag< by_id >, member< pod_object, pod_object_id_type, &pod_object::id > >,
        ordered_unique< tag< by_name >, member< pod_object, pod_name_type, &pod_object::name > >
    >,
    allocator< pod_object >
> pod_index;
// clang-format on

//------------------------------------------------------------------------------

class pod_join_request_object : public object<pod_join_request_object_type, pod_join_request_object> {
  pod_join_request_object() = delete;

 public:
  template <typename Constructor, typename Allocator>
  pod_join_request_object(Constructor&& c, allocator<Allocator> a) : memo(a) {
      c(*this);
  };

  id_type id;

  account_name_type account;         /// account
  pod_name_type pod;                 /// pod
  time_point_sec created;            /// when this request is created
  asset fee = asset(0, WLS_SYMBOL);  /// fee to join this pod
  shared_string memo;                /// The memo is plain-text, any encryption on the memo is up to a higher level protocol.
};

/**
 * pod_join_request_pending_index
 *
 * this index holds pending requests only, these requests not resolved yet (accepted or rejected) by pod owner.
 * object should be removed from this index when resolved
 */

// clang-format off
struct by_pod_account;
struct by_account_pod;

typedef multi_index_container<
    pod_join_request_object,
    indexed_by<
        ordered_unique< tag< by_id >, member< pod_join_request_object, pod_join_request_object_id_type, &pod_join_request_object::id > >,
        ordered_unique< tag< by_pod_account >,
          composite_key< pod_join_request_object,
             member< pod_join_request_object, pod_name_type, &pod_join_request_object::pod >,
             member< pod_join_request_object, account_name_type, &pod_join_request_object::account >
          >, /// composite key by_pod_account
          composite_key_compare < std::less<pod_name_type>, std::less<account_name_type> >
        >
        /// NON_CONSENSUS INDICES - used by APIs
#ifndef IS_LOW_MEM
        ,
        ordered_unique< tag< by_account_pod >,
          composite_key< pod_join_request_object,
             member< pod_join_request_object, account_name_type, &pod_join_request_object::account >,
             member< pod_join_request_object, pod_name_type, &pod_join_request_object::pod >
          >, /// composite key by_account_pod
          composite_key_compare < std::less<account_name_type>, std::less<pod_name_type> >
        >
#endif
    >,
    allocator< pod_join_request_object >
> pod_join_request_pending_index;
// clang-format on


//------------------------------------------------------------------------------

class pod_member_object : public object<pod_member_object_type, pod_member_object> {
  pod_member_object() = delete;

 public:
  template <typename Constructor, typename Allocator>
  pod_member_object(Constructor&& c, allocator<Allocator> a) {
    c(*this);
  };

  id_type id;

  pod_name_type pod;          /// pod
  account_name_type account;  /// account
  time_point_sec joined;      /// when joined

  // uint8_t rank = 0; // <== dont need, should be calculated dynamically using stats

  uint32_t total_posts = 0;     /// root-post only (no comments)
  uint32_t total_comments = 0;  /// comments only (no root-posts included)
};

// clang-format off
struct by_pod_account;
struct by_account_pod;

typedef multi_index_container<
    pod_member_object,
    indexed_by<
        ordered_unique< tag< by_id >, member< pod_member_object, pod_member_object_id_type, &pod_member_object::id > >,
        ordered_unique< tag< by_pod_account >,
          composite_key< pod_member_object,
             member< pod_member_object, pod_name_type, &pod_member_object::pod >,
             member< pod_member_object, account_name_type, &pod_member_object::account >
          >, /// composite key by_pod_account
          composite_key_compare < std::less<pod_name_type>, std::less<account_name_type> >
        >
        /// NON_CONSENSUS INDICES - used by APIs
#ifndef IS_LOW_MEM
        ,
        ordered_unique< tag< by_account_pod >,
          composite_key< pod_member_object,
             member< pod_member_object, account_name_type, &pod_member_object::account >,
             member< pod_member_object, pod_name_type, &pod_member_object::pod >
          >, /// composite key by_account_pod
          composite_key_compare < std::less<account_name_type>, std::less<pod_name_type> >
        >
#endif
    >,
    allocator< pod_member_object >
> pod_member_index;
// clang-format on

}  // namespace chain
}  // namespace wls

FC_REFLECT(wls::chain::pod_object, (id)(name)(created)(fee)(json_metadata)(allow_join)(member_count))
CHAINBASE_SET_INDEX_TYPE(wls::chain::pod_object, wls::chain::pod_index)

FC_REFLECT(wls::chain::pod_join_request_object, (id)(account)(pod)(created)(fee)(memo))
CHAINBASE_SET_INDEX_TYPE(wls::chain::pod_join_request_object, wls::chain::pod_join_request_pending_index)

FC_REFLECT(wls::chain::pod_member_object, (id)(pod)(account)(joined)(total_posts)(total_comments))
CHAINBASE_SET_INDEX_TYPE(wls::chain::pod_member_object, wls::chain::pod_member_index)