#pragma once

#include <wls/protocol/asset.hpp>

namespace wls {
namespace chain {
namespace util {

using wls::protocol::asset;
using wls::protocol::price;

}  // namespace util
}  // namespace chain
}  // namespace wls
