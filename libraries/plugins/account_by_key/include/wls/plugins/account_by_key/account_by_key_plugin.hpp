#pragma once

#include <appbase/application.hpp>

#include <wls/plugins/chain/chain_plugin.hpp>

#define ACCOUNT_BY_KEY_PLUGIN_NAME "account_by_key"

namespace wls {
namespace plugins {
namespace account_by_key {

using namespace appbase;

namespace detail {
class account_by_key_plugin_impl;
}

class account_by_key_plugin : public appbase::plugin<account_by_key_plugin> {
 public:
  account_by_key_plugin();

  virtual ~account_by_key_plugin();

  APPBASE_PLUGIN_REQUIRES((wls::plugins::chain::chain_plugin))

  static const std::string &name() {
    static std::string name = ACCOUNT_BY_KEY_PLUGIN_NAME;
    return name;
  }

  virtual void set_program_options(options_description &cli, options_description &cfg) override;

  virtual void plugin_initialize(const variables_map &options) override;

  virtual void plugin_startup() override;

  virtual void plugin_shutdown() override;

  friend class detail::account_by_key_plugin_impl;

 private:
  std::unique_ptr<detail::account_by_key_plugin_impl> my;
};

}  // namespace account_by_key
}  // namespace plugins
}  // namespace wls
