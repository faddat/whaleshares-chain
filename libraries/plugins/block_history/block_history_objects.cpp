#include <wls/plugins/block_history/block_history_objects.hpp>
#include <wls/chain/history_object.hpp>

namespace wls {
namespace plugins {
namespace block_history {

block_history_applied_operation::block_history_applied_operation() {}

block_history_applied_operation::block_history_applied_operation(const operation_object &op_obj)
    : trx_id(op_obj.trx_id),
      block(op_obj.block),
      trx_in_block(op_obj.trx_in_block),
      op_in_trx(op_obj.op_in_trx),
      virtual_op(op_obj.virtual_op),
      timestamp(op_obj.timestamp) {
  // fc::raw::unpack( op_obj.serialized_op, op );     // g++ refuses to compile this as ambiguous
  op = fc::raw::unpack<wls::protocol::operation>(op_obj.serialized_op);
}

}  // namespace block_history
}  // namespace plugins
}  // namespace wls