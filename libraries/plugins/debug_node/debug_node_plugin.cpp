#include <appbase/application.hpp>
#include <appbase/plugin.hpp>

#include <wls/chain/witness_objects.hpp>
#include <wls/utilities/key_conversion.hpp>
#include <wls/plugins/debug_node/debug_node_plugin.hpp>
#include <wls/plugins/chain/chain_plugin.hpp>

#include <fc/io/buffered_iostream.hpp>
#include <fc/io/fstream.hpp>
#include <fc/io/json.hpp>

#include <fc/thread/future.hpp>
#include <fc/thread/mutex.hpp>
#include <fc/thread/scoped_lock.hpp>

#include <sstream>
#include <string>

namespace wls {
namespace plugins {
namespace debug_node {

namespace detail {
class debug_node_plugin_impl {
 public:
  debug_node_plugin_impl(debug_node_plugin &_plugin) : _db(appbase::app().get_plugin<plugins::chain::chain_plugin>().db()), _self(_plugin) {}

  virtual ~debug_node_plugin_impl();

  void on_applied_block(const protocol::signed_block &b);

  void on_changed_objects(const std::vector<graphene::db::object_id_type> &ids);

  void on_removed_objects(const std::vector<const graphene::db::object *> objs);

  void apply_debug_updates();

  chain::database &_db;

  debug_node_plugin &_self;

  uint32_t _mining_threads = 1;
  std::vector<std::shared_ptr<fc::thread> > _thread_pool;

  boost::signals2::scoped_connection _applied_block_conn;

  std::map<protocol::block_id_type, std::vector<std::function<void(chain::database &)> > > _debug_updates;
};

debug_node_plugin_impl::~debug_node_plugin_impl() {}

void debug_node_plugin_impl::on_applied_block(const chain::signed_block &b) {
  try {
    if (!_debug_updates.empty()) apply_debug_updates();
  }
  FC_LOG_AND_RETHROW()
}

void debug_node_plugin_impl::apply_debug_updates() {
  // this was a method on database in Graphene
  chain::block_id_type head_id = _db.head_block_id();
  auto it = _debug_updates.find(head_id);
  if (it == _debug_updates.end()) return;

  for (auto &update : it->second) update(_db);
}

}  // namespace detail

private_key_storage::private_key_storage() {}

private_key_storage::~private_key_storage() {}

debug_node_plugin::debug_node_plugin() {}

debug_node_plugin::~debug_node_plugin() {}

void debug_node_plugin::set_program_options(boost::program_options::options_description &cli, boost::program_options::options_description &cfg) {
  // clang-format off
  cli.add_options()(
    "edit-script,e", boost::program_options::value<std::vector<std::string> >()->composing(),
    "Database edits to apply on startup (may specify multiple times)"
  );
  // clang-format on
  cfg.add(cli);
}

void debug_node_plugin::plugin_initialize(const boost::program_options::variables_map &options) {
  ilog("Initializing debug_node plugin");

  _my = std::make_shared<detail::debug_node_plugin_impl>(*this);

  // connect needed signals
  _my->_applied_block_conn = _my->_db.applied_block.connect([&](const chain::signed_block &b) { _my->on_applied_block(b); });

  if (options.count("edit-script") > 0) {
    _edit_scripts = options.at("edit-script").as<std::vector<std::string> >();
  }

  if (options.count("mining-threads") > 0) {
    _my->_mining_threads = options.at("mining-threads").as<uint32_t>();
  }

  if (logging) wlog("Initializing ${n} mining threads", ("n", _my->_mining_threads));
  _my->_thread_pool.resize(_my->_mining_threads);
  for (uint32_t i = 0; i < _my->_mining_threads; ++i) _my->_thread_pool[i] = std::make_shared<fc::thread>();
}

void debug_node_plugin::plugin_startup() {
  if (logging) ilog("debug_node_plugin::plugin_startup() begin");
}

uint32_t debug_node_plugin::debug_generate_blocks(const std::string &debug_key, uint32_t count, uint32_t skip, uint32_t miss_blocks,
                                                  private_key_storage *key_storage) {
  if (count == 0) return 0;

  fc::optional<fc::ecc::private_key> debug_private_key;
  wls::chain::public_key_type debug_public_key;
  if (debug_key != "") {
    debug_private_key = wls::utilities::wif_to_key(debug_key);
    FC_ASSERT(debug_private_key.valid());
    debug_public_key = debug_private_key->get_public_key();
  }

  uint32_t slot = miss_blocks + 1, produced = 0;
  while (produced < count) {
    uint32_t new_slot = miss_blocks + 1;
    std::string scheduled_witness_name = _my->_db.get_scheduled_witness(slot);
    fc::time_point_sec scheduled_time = _my->_db.get_slot_time(slot);
    const chain::witness_object &scheduled_witness = _my->_db.get_witness(scheduled_witness_name);
    wls::chain::public_key_type scheduled_key = scheduled_witness.signing_key;
    if (debug_key != "") {
      if (logging) wlog("scheduled key is: ${sk}   dbg key is: ${dk}", ("sk", scheduled_key)("dk", debug_public_key));
      if (scheduled_key != debug_public_key) {
        if (logging) wlog("Modified key for witness ${w}", ("w", scheduled_witness_name));
        debug_update(
            [=](chain::database &db) {
              db.modify(db.get_witness(scheduled_witness_name), [&](chain::witness_object &w) { w.signing_key = debug_public_key; });
            },
            skip);
      }
    } else {
      debug_private_key.reset();
      if (key_storage != nullptr) key_storage->maybe_get_private_key(debug_private_key, scheduled_key, scheduled_witness_name);
      if (!debug_private_key.valid()) {
        if (logging) elog("Skipping ${wit} because I don't know the private key", ("wit", scheduled_witness_name));
        new_slot = slot + 1;
        FC_ASSERT(slot < miss_blocks + 50);
      }
    }
    _my->_db.generate_block(scheduled_time, scheduled_witness_name, *debug_private_key, skip);
    ++produced;
    slot = new_slot;
  }

  return count;
}

uint32_t debug_node_plugin::debug_generate_blocks_until(const std::string &debug_key, const fc::time_point_sec &head_block_time, bool generate_sparsely,
                                                        uint32_t skip, private_key_storage *key_storage) {
  if (_my->_db.head_block_time() >= head_block_time) return 0;

  uint32_t new_blocks = 0;

  if (generate_sparsely) {
    new_blocks += debug_generate_blocks(debug_key, 1, skip);
    auto slots_to_miss = _my->_db.get_slot_at_time(head_block_time);
    if (slots_to_miss > 1) {
      slots_to_miss--;
      new_blocks += debug_generate_blocks(debug_key, 1, skip, slots_to_miss, key_storage);
    }
  } else {
    while (_my->_db.head_block_time() < head_block_time) new_blocks += debug_generate_blocks(debug_key, 1);
  }

  return new_blocks;
}

void debug_node_plugin::plugin_shutdown() {
  ilog("debug_node_plugin::plugin_shutdown");
  _my->_applied_block_conn.disconnect();
  _my->_debug_updates.clear();

  return;
}

void debug_node_plugin::debug_update(std::function<void(wls::chain::database &)> callback, uint32_t skip /* = wls::chain::database::skip_nothing */) {
  // this was a method on database in Graphene
  chain::block_id_type head_id = _my->_db.head_block_id();
  auto it = _my->_debug_updates.find(head_id);
  if (it == _my->_debug_updates.end()) it = _my->_debug_updates.emplace(head_id, std::vector<std::function<void(chain::database &)> >()).first;
  it->second.emplace_back(callback);

  fc::optional<chain::signed_block> head_block = _my->_db.fetch_block_by_id(head_id);
  FC_ASSERT(head_block.valid());

  // What the last block does has been changed by adding to node_property_object, so we have to re-apply it
  _my->_db.pop_block();
  _my->_db.push_block(*head_block, skip);
}
}  // namespace debug_node
}  // namespace plugins
}  // namespace wls
