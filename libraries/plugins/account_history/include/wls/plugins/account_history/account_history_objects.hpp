#pragma once

#include <wls/chain/wls_object_types.hpp>
#include <wls/plugins/block_history/block_history_objects.hpp>
#include <boost/multi_index/composite_key.hpp>

namespace wls {
namespace plugins {
namespace account_history {

using namespace std;
using namespace wls::chain;
using namespace boost::multi_index;

class account_history_key {
 public:
  account_history_key(const account_name_type &_account, const uint32_t _sequence)
      : account(_account), sequence(_sequence) {}

  account_history_key() {}

  account_name_type account;
  uint32_t sequence = 0;
};

typedef wls::plugins::block_history::block_history_applied_operation account_history_object;

//------------------------------------------------------------------------------

#ifndef ACCOUNT_HISTORY_SPACE_ID
#define ACCOUNT_HISTORY_SPACE_ID 19
#endif

enum account_history_object_types {
  account_history_state_object_type = (ACCOUNT_HISTORY_SPACE_ID << 8)
};

/**
 * to track last sequence of an account ( the head of the snake )
 */
class account_history_state_head_object : public object<account_history_state_object_type, account_history_state_head_object> {
 public:
  template <typename Constructor, typename Allocator>
  account_history_state_head_object(Constructor &&c, allocator<Allocator> a) {
    c(*this);
  }

  id_type id;

  account_name_type account;
  uint32_t sequence = 0;
};

//------------------------------------------------------------------------------

typedef account_history_state_head_object::id_type account_history_state_head_object_id_type;

// clang-format off
struct by_account;
typedef multi_index_container<
    account_history_state_head_object,
    indexed_by<
        ordered_unique< tag< by_id >, member< account_history_state_head_object, account_history_state_head_object_id_type, &account_history_state_head_object::id > >,
        ordered_unique< tag< by_account >, member< account_history_state_head_object, account_name_type, &account_history_state_head_object::account > >
    >,
    allocator< account_history_state_head_object >
> account_history_state_head_object_index;
// clang-format on


}  // namespace account_history
}  // namespace plugins
}  // namespace wls

FC_REFLECT(wls::plugins::account_history::account_history_key, (account)(sequence))
//FC_REFLECT(wls::plugins::account_history::account_history_object, (author)(permlink)(shared_by))
FC_REFLECT(wls::plugins::account_history::account_history_state_head_object, (id)(account)(sequence))
CHAINBASE_SET_INDEX_TYPE(wls::plugins::account_history::account_history_state_head_object, wls::plugins::account_history::account_history_state_head_object_index)
