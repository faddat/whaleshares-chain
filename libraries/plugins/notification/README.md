# notification

notification types:
- transfer
- transfer_to_vesting
- claim_daily_reward
- htlc
- friend/unfriend
- pod/join/accept/kich/cancel
- tip(comment/user)
- reply
- witness-vote/unvote
- follow/unfollow
- mention