#pragma once

#include <appbase/application.hpp>

#include <wls/plugins/chain/chain_plugin.hpp>
#include <wls/plugins/block_history/block_history_plugin.hpp>
#include <wls/plugins/notification/notification_objects.hpp>
#include <wls/nudbstore/nudbstore.hpp>

#define NOTIFICATION_PLUGIN_NAME "notification"

namespace wls {
namespace plugins {
namespace notification {

namespace detail {
class notification_plugin_impl;
}

using namespace appbase;
using wls::protocol::account_name_type;
using nudbstore::nudbstore;

class notification_plugin : public plugin<notification_plugin> {
 public:
  notification_plugin();

  virtual ~notification_plugin();

  APPBASE_PLUGIN_REQUIRES((wls::plugins::chain::chain_plugin)(wls::plugins::block_history::block_history_plugin))

  static const std::string &name() {
    static std::string name = NOTIFICATION_PLUGIN_NAME;
    return name;
  }

  virtual void set_program_options(options_description &cli, options_description &cfg) override;

  virtual void plugin_initialize(const variables_map &options) override;

  virtual void plugin_startup() override;

  virtual void plugin_shutdown() override;

  const notification_state_head_object* get_notification_state_head_object(const account_name_type &account);

  nudbstore<notification_key, notification_object>& get_nudbstore_database();

  // friend class detail::notification_plugin_impl;

 private:
  std::unique_ptr<detail::notification_plugin_impl> my;
};

}  // namespace notification
}  // namespace plugins
}  // namespace wls
