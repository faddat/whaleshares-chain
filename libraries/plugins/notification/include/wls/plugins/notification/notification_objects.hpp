#pragma once

#include <wls/chain/wls_object_types.hpp>
#include <wls/plugins/block_history/block_history_objects.hpp>
#include <boost/multi_index/composite_key.hpp>

namespace wls {
namespace plugins {
namespace notification {

using namespace std;
using namespace wls::chain;
using namespace boost::multi_index;

class notification_key {
 public:
  notification_key(const account_name_type &_account, const uint32_t _sequence)
      : account(_account), sequence(_sequence) {}

  notification_key() {}

  account_name_type account;
  uint32_t sequence = 0;
};

typedef wls::plugins::block_history::block_history_applied_operation notification_object;

//------------------------------------------------------------------------------

#ifndef NOTIFICATION_SPACE_ID
#define NOTIFICATION_SPACE_ID 22
#endif

enum notification_object_types {
  notification_state_object_type = (NOTIFICATION_SPACE_ID << 8)
};

/**
 * to track last sequence of an account ( the head of the snake )
 */
class notification_state_head_object : public object<notification_state_object_type, notification_state_head_object> {
 public:
  template <typename Constructor, typename Allocator>
  notification_state_head_object(Constructor &&c, allocator<Allocator> a) {
    c(*this);
  }

  id_type id;

  account_name_type account;
  uint32_t sequence = 0;
};

//------------------------------------------------------------------------------

typedef notification_state_head_object::id_type notification_state_head_object_id_type;

// clang-format off
struct by_account;
typedef multi_index_container<
    notification_state_head_object,
    indexed_by<
        ordered_unique< tag< by_id >, member< notification_state_head_object, notification_state_head_object_id_type, &notification_state_head_object::id > >,
        ordered_unique< tag< by_account >, member< notification_state_head_object, account_name_type, &notification_state_head_object::account > >
    >,
    allocator< notification_state_head_object >
> notification_state_head_object_index;
// clang-format on


}  // namespace notification
}  // namespace plugins
}  // namespace wls

FC_REFLECT(wls::plugins::notification::notification_key, (account)(sequence))
FC_REFLECT(wls::plugins::notification::notification_state_head_object, (id)(account)(sequence))
CHAINBASE_SET_INDEX_TYPE(wls::plugins::notification::notification_state_head_object, wls::plugins::notification::notification_state_head_object_index)
