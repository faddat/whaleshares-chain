#pragma once

#include <cstdint>

namespace wls {
namespace plugins {
namespace witness {

/**
 * simple daily bw based on WHALESTAKE:
 * - break into ranges based on log10 scale (0-10, 10-100, 100-1K...)
 * - then find the max-threshold in linear within that range.
 */
int64_t simple_daily_bandwidth_limit(int64_t stake, bool has_hf3 = true);


}  // namespace witness
}  // namespace plugins
}  // namespace wls
