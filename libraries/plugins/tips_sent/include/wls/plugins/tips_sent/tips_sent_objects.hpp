#pragma once

#include <wls/chain/wls_object_types.hpp>
#include <wls/plugins/block_history/block_history_objects.hpp>
#include <boost/multi_index/composite_key.hpp>

namespace wls {
namespace plugins {
namespace tips_sent {

using namespace std;
using namespace wls::chain;
using namespace boost::multi_index;

class tips_sent_key {
 public:
  tips_sent_key(const account_name_type &_account, const uint32_t _sequence)
      : account(_account), sequence(_sequence) {}

  tips_sent_key() {}

  account_name_type account;
  uint32_t sequence = 0;
};

//class tips_sent_object {
// public:
//  tips_sent_object() {}
//
//  tips_sent_object(const account_name_type &_author, const std::string &permlink) : author(_author), permlink(permlink) {}
//
//  account_name_type author;
//  std::string permlink;
//
//  std::set<std::string> shared_by;
//
//
//  social_action_user_tip;
//};
typedef wls::plugins::block_history::block_history_applied_operation tips_sent_object;

//------------------------------------------------------------------------------

#ifndef TIPS_SENT_SPACE_ID
#define TIPS_SENT_SPACE_ID 20
#endif

enum tips_sent_object_types {
  tips_sent_state_object_type = (TIPS_SENT_SPACE_ID << 8)
};

/**
 * to track last sequence of an account ( the head of the snake )
 */
class tips_sent_state_head_object : public object<tips_sent_state_object_type, tips_sent_state_head_object> {
 public:
  template <typename Constructor, typename Allocator>
  tips_sent_state_head_object(Constructor &&c, allocator<Allocator> a) {
    c(*this);
  }

  id_type id;

  account_name_type account;
  uint32_t sequence = 0;
};

//------------------------------------------------------------------------------

typedef tips_sent_state_head_object::id_type tips_sent_state_head_object_id_type;

// clang-format off
struct by_account;
typedef multi_index_container<
    tips_sent_state_head_object,
    indexed_by<
        ordered_unique< tag< by_id >, member< tips_sent_state_head_object, tips_sent_state_head_object_id_type, &tips_sent_state_head_object::id > >,
        ordered_unique< tag< by_account >, member< tips_sent_state_head_object, account_name_type, &tips_sent_state_head_object::account > >
    >,
    allocator< tips_sent_state_head_object >
> tips_sent_state_head_object_index;
// clang-format on


}  // namespace tips_sent
}  // namespace plugins
}  // namespace wls

FC_REFLECT(wls::plugins::tips_sent::tips_sent_key, (account)(sequence))
//FC_REFLECT(wls::plugins::tips_sent::tips_sent_object, (author)(permlink)(shared_by))
FC_REFLECT(wls::plugins::tips_sent::tips_sent_state_head_object, (id)(account)(sequence))
CHAINBASE_SET_INDEX_TYPE(wls::plugins::tips_sent::tips_sent_state_head_object, wls::plugins::tips_sent::tips_sent_state_head_object_index)
