#pragma once

#include <appbase/application.hpp>
#include <wls/plugins/chain/chain_plugin.hpp>

#define SNAPSHOT_PLUGIN_NAME "snapshot"

namespace wls {
namespace plugins {
namespace snapshot {

namespace detail {
class snapshot_plugin_impl;
}

using namespace appbase;
using wls::protocol::account_name_type;

class snapshot_plugin : public plugin<snapshot_plugin> {
 public:
  snapshot_plugin();

  virtual ~snapshot_plugin();

  APPBASE_PLUGIN_REQUIRES((wls::plugins::chain::chain_plugin))

  static const std::string &name() {
    static std::string name = SNAPSHOT_PLUGIN_NAME;
    return name;
  }

  virtual void set_program_options(options_description &cli, options_description &cfg) override;

  virtual void plugin_initialize(const variables_map &options) override;

  virtual void plugin_startup() override;

  virtual void plugin_shutdown() override;

  // friend class detail::snapshot_plugin_impl;

 private:
  std::unique_ptr<detail::snapshot_plugin_impl> my;
};

}  // namespace snapshot
}  // namespace plugins
}  // namespace wls
