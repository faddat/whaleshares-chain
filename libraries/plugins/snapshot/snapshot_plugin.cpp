#include <wls/plugins/snapshot/snapshot_plugin.hpp>
#include <wls/chain/wls_objects.hpp>
#include <wls/chain/database.hpp>
#include <wls/chain/index.hpp>
#include <wls/chain/operation_notification.hpp>
#include <wls/chain/util/signal.hpp>
#include <wls/utilities/plugin_utilities.hpp>

#include <fc/smart_ref_impl.hpp>
#include <fc/thread/thread.hpp>
#include <fc/filesystem.hpp>
#include <fc/io/raw.hpp>
#include <fc/exception/exception.hpp>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

#include <iostream>

namespace wls {
namespace plugins {
namespace snapshot {

using namespace wls::protocol;

using chain::database;
using chain::operation_notification;

namespace detail {

namespace bfs = boost::filesystem;

class snapshot_plugin_impl {
 public:
  snapshot_plugin_impl(snapshot_plugin &_plugin, string path)
      : _db(appbase::app().get_plugin<wls::plugins::chain::chain_plugin>().db()), _self(_plugin), _path(path) {
  }

  virtual ~snapshot_plugin_impl() {}

  void on_irreversible_block( uint32_t block_num );

  //--
  database &_db;
  snapshot_plugin &_self;
  string _path; // /plugins/snapshot/

  boost::signals2::connection _irreversible_block_con;
};

void snapshot_plugin_impl::on_irreversible_block(uint32_t block_num) {
  try {
    if (block_num % 1000000 != 0) return;

    ilog("snapshot at block ${b}", ("b", block_num));

    string filepath = _path + std::to_string(block_num) + ".txt";
    if (fc::exists(filepath)) return;  // ignore if snapshot created already

    bfs::path p{filepath};
    bfs::ofstream ofs{p};
    const auto &props = _db.get_dynamic_global_properties();
    const auto& account_idx = _db.get_index<wls::chain::account_index>().indices();

    for (auto itr = account_idx.begin(); itr != account_idx.end(); ++itr) {
      const auto& a = *itr;
      auto whalestake = a.vesting_shares * props.get_vesting_share_price();
      ofs << fc::json::to_string(a.name);
      ofs << ", " << fc::json::to_string(whalestake);
      ofs << std::endl;
    }

    ofs.close();
  }
  FC_CAPTURE_AND_RETHROW()
}

}  // end namespace detail

snapshot_plugin::snapshot_plugin() {}

snapshot_plugin::~snapshot_plugin() {}

void snapshot_plugin::set_program_options(options_description &cli, options_description &cfg) {
  // clang-format off
//  cli.add_options()(
//  );
  // clang-format on
  cfg.add(cli);
}

void snapshot_plugin::plugin_initialize(const boost::program_options::variables_map &options) {
  try {
    ilog("Initializing snapshot plugin");

    string data_dir = appbase::app().data_dir().string();
    if (data_dir == "") data_dir = ".";
    string path = data_dir + "/plugins/snapshot/";
    if (!fc::exists(path)) boost::filesystem::create_directories(path);

    my = std::make_unique<detail::snapshot_plugin_impl>(*this, path);

    my->_irreversible_block_con = my->_db.on_irreversible_block.connect([&](uint32_t block_num) { my->on_irreversible_block(block_num); });

  } FC_CAPTURE_AND_RETHROW()
}

void snapshot_plugin::plugin_startup() {
  try {
    ilog("snapshot plugin: plugin_startup()");
  } FC_CAPTURE_AND_RETHROW()
}

void snapshot_plugin::plugin_shutdown() {
  try {
    ilog("snapshot::plugin_shutdown");
    chain::util::disconnect_signal(my->_irreversible_block_con);
  } FC_CAPTURE_AND_RETHROW()
}

}  // namespace snapshot
}  // namespace plugins
}  // namespace wls
