file(GLOB HEADERS "include/wls/plugins/snapshot/*.hpp")

add_library(snapshot_plugin
        snapshot_plugin.cpp
        )

target_link_libraries(snapshot_plugin chain_plugin wls_chain wls_protocol wls_utilities)
target_include_directories(snapshot_plugin PUBLIC
        "${CMAKE_CURRENT_SOURCE_DIR}/include"
        )

install(TARGETS
        snapshot_plugin

        RUNTIME DESTINATION bin
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib
        )
