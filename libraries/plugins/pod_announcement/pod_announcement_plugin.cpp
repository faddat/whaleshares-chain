#include <wls/plugins/pod_announcement/pod_announcement_plugin.hpp>
#include <wls/plugins/pod_announcement/pod_announcement_objects.hpp>
#include <wls/plugins/block_history/block_history_objects.hpp>
#include <wls/plugins/follow/follow_objects.hpp>
#include <wls/chain/wls_objects.hpp>
#include <wls/chain/database.hpp>
#include <wls/chain/index.hpp>
#include <wls/chain/operation_notification.hpp>
#include <wls/chain/history_object.hpp>
#include <wls/chain/util/impacted.hpp>
#include <wls/chain/util/signal.hpp>
#include <wls/utilities/plugin_utilities.hpp>

#include <fc/smart_ref_impl.hpp>
#include <fc/thread/thread.hpp>

#include <boost/algorithm/string.hpp>

namespace wls {
namespace plugins {
namespace pod_announcement {

using namespace wls::protocol;

using chain::database;
using chain::operation_notification;

namespace detail {

class pod_announcement_plugin_impl {
 public:
  pod_announcement_plugin_impl(pod_announcement_plugin &_plugin, const string &path)
      : _db(appbase::app().get_plugin<wls::plugins::chain::chain_plugin>().db()), _self(_plugin), _nudbstore_database{path} {
    _block_history_plugin = appbase::app().find_plugin<wls::plugins::block_history::block_history_plugin>();
  }

  virtual ~pod_announcement_plugin_impl() {}

  void on_irreversible_block( uint32_t block_num );

  /**
   * high level insert which appends item to feed and updates head state
   * @param pod
   * @param item
   */
  void insert(const pod_name_type &pod, const pod_announcement_object &item);

  const pod_announcement_state_head_object* get_pod_announcement_state_head_object(const pod_name_type &pod);

  //--

  database &_db;
  pod_announcement_plugin &_self;

  nudbstore<pod_announcement_key, pod_announcement_object> _nudbstore_database;
  boost::signals2::connection _irreversible_block_con;

  wls::plugins::block_history::block_history_plugin *_block_history_plugin;
};

const pod_announcement_state_head_object *pod_announcement_plugin_impl::get_pod_announcement_state_head_object(const pod_name_type &pod) {
  try {
    return _db.find<pod_announcement_state_head_object, by_pod>(pod);
  }
  FC_CAPTURE_AND_RETHROW((pod))
}

void pod_announcement_plugin_impl::insert(const pod_name_type &pod, const pod_announcement_object &item) {
  auto head = this->get_pod_announcement_state_head_object(pod);
  if (head == nullptr) {
    head = &_db.create<pod_announcement_state_head_object>([&](pod_announcement_state_head_object &obj) { obj.pod = pod; });
  }

  // insert
  pod_announcement_key new_key{pod, head->sequence};
//  ilog("new_key: ${new_key}, item: ${item}", ("new_key", new_key)("item", item));
  boost::system::error_code ec;
  _nudbstore_database.insert(new_key, item, ec);
  if (ec) {
    if ((ec == nudb::error::key_exists) && (ec.category() == nudb::nudb_category())) {
      // ignore key_exists
    } else {
      FC_ASSERT(true, "${msg}", ("msg", ec.message()));
    }
  }

  // update last sequence to pod head state
  _db.modify(*head, [&](pod_announcement_state_head_object& obj) { obj.sequence++; });
}

struct operation_visitor {
  operation_visitor(pod_announcement_plugin_impl &plugin) : _plugin(plugin) {}

  typedef void result_type;

  pod_announcement_plugin_impl &_plugin;

  comment_metadata filter_tags(const comment_object &c) const {
    comment_metadata meta;

    if (c.json_metadata.size()) {
      try {
        meta = fc::json::from_string(to_string(c.json_metadata)).as<comment_metadata>();
      } catch (const fc::exception &e) {
        // Do nothing on malformed json_metadata
      }
    }

    // We need to write the transformed tags into a temporary
    // local container because we can't modify meta.tags concurrently
    // as we iterate over it.
    set<string> lower_tags;

    uint8_t tag_limit = 5;
    uint8_t count = 0;
    for (const string &tag : meta.tags) {
      ++count;
      if (count > tag_limit || lower_tags.size() > tag_limit) break;
      if (tag == "") continue;
      lower_tags.insert(fc::to_lower(tag));
    }

    meta.tags = lower_tags;  /// TODO: std::move???

    return meta;
  }

  template <typename T>
  void operator()(const T &) const {}

  void operator()(const social_action_operation &op) const {
    switch (op.action.which()) {
      case social_action::tag<social_action_comment_create>::value: {
        auto action = op.action.get<social_action_comment_create>();
        if (action.parent_author != WLS_ROOT_POST_PARENT) return;  // must be root post
        if (!action.pod) return;                                   // must be pod post
        if (*action.pod == WLS_ROOT_POST_PARENT) return;           // must be pod post
        if (*action.pod != op.account) return;                     // author must be pod owner

        const auto *comment = _plugin._db.find_comment(op.account, action.permlink);
        if (comment == nullptr) return;

        { // must contains announcement tag
          comment_metadata meta = filter_tags(*comment);
          std::set<std::string>::iterator it = meta.tags.find("announcement");
          if (it == meta.tags.end())  return;
        }

        pod_announcement_object feed_item(comment->author, chain::to_string(comment->permlink));
        _plugin.insert(*action.pod, feed_item);
      } break;
    }
  }
};

void pod_announcement_plugin_impl::on_irreversible_block(uint32_t block_num) {
  FC_ASSERT(_block_history_plugin != nullptr, "block_history_plugin is required for pod_announcement_plugin");

  try {
    vector<wls::plugins::block_history::block_history_operation_object> ops = _block_history_plugin->get_ops_recent_block(block_num);
    for (const auto &item : ops) {
      operation op = fc::raw::unpack<operation>(item.serialized_op);
      op.visit(operation_visitor(*this));
    }
  }
  FC_CAPTURE_AND_RETHROW()
}

}  // end namespace detail

pod_announcement_plugin::pod_announcement_plugin() {}

pod_announcement_plugin::~pod_announcement_plugin() {}

void pod_announcement_plugin::set_program_options(options_description &cli, options_description &cfg) {
  // clang-format off
//  cli.add_options()(
//  );
  // clang-format on
  cfg.add(cli);
}

void pod_announcement_plugin::plugin_initialize(const boost::program_options::variables_map &options) {
  try {
    ilog("pod_announcement_plugin::plugin_initialize");

    string data_dir = appbase::app().data_dir().string();
    if (data_dir == "") data_dir = ".";
    string path = data_dir + "/plugins/pod_announcement/";
    my = std::make_unique<detail::pod_announcement_plugin_impl>(*this, path);

    {
      bool replay = options.at("replay-blockchain").as<bool>();
      bool replay_blockchain = options.at("replay-blockchain").as<bool>();
      bool resync_blockchain = options.at("resync-blockchain").as<bool>();

      if (replay || replay_blockchain || resync_blockchain) my->_nudbstore_database.clean();
    }

    my->_nudbstore_database.open();
    my->_irreversible_block_con = my->_db.on_irreversible_block.connect([&](uint32_t block_num) { my->on_irreversible_block(block_num); });

    wls::chain::add_plugin_index<pod_announcement_state_head_object_index>(my->_db);

  } FC_CAPTURE_AND_RETHROW()
}

void pod_announcement_plugin::plugin_startup() {
  try {
    ilog("pod_announcement_plugin::plugin_startup");
  } FC_CAPTURE_AND_RETHROW()
}

void pod_announcement_plugin::plugin_shutdown() {
  try {
    ilog("pod_announcement::plugin_shutdown");
    chain::util::disconnect_signal(my->_irreversible_block_con);

    my->_nudbstore_database.close();
  } FC_CAPTURE_AND_RETHROW()
}

nudbstore<pod_announcement_key, pod_announcement_object>& pod_announcement_plugin::get_nudbstore_database() {
  return my->_nudbstore_database;
}

const pod_announcement_state_head_object *pod_announcement_plugin::get_pod_announcement_state_head_object(const pod_name_type &pod) {
  return my->get_pod_announcement_state_head_object(pod);
}

}  // namespace pod_announcement
}  // namespace plugins
}  // namespace wls
