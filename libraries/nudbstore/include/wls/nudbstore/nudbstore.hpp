#pragma once

#include <wls/chain/wls_object_types.hpp>
#include <fc/exception/exception.hpp>
#include <fc/filesystem.hpp>
#include <fc/io/raw.hpp>
#include <boost/filesystem.hpp>

#include <nudb/nudb.hpp>

#include <string>

namespace wls {
namespace plugins {
namespace nudbstore {

using std::string;
using boost::system::error_code;

auto const nudbstore_dat_path = "db.dat";
auto const nudbstore_key_path = "db.key";
auto const nudbstore_log_path = "db.log";

template <typename K, typename V>
class nudbstore {
 public:
  nudbstore(const string &path) : datapath(path){}

  virtual ~nudbstore() {}

  void open() {
    error_code ec;

    if (!fc::exists(datapath)) {
      boost::filesystem::create_directories(datapath);
      //auto key_size = K().data_size();
      std::size_t key_size = 20; // fixed 20 as ripemd160
      uint64_t salt = nudb::make_salt();
      uint64_t appnum = 0;
      auto blockSize = nudb::block_size(".");
      float load_factor = 0.5f;
      // clang-format off
      nudb::create<nudb::xxhasher>(
          datapath + nudbstore_dat_path,
          datapath + nudbstore_key_path,
          datapath + nudbstore_log_path,
          appnum,
          salt,
          key_size,
          blockSize,
          load_factor,
          ec);
      // clang-format on
      FC_ASSERT(!ec, "${msg}", ("msg", ec.message()));
    }

    _store.open(datapath + nudbstore_dat_path, datapath + nudbstore_key_path, datapath + nudbstore_log_path, ec);
    FC_ASSERT(!ec, "${msg}", ("msg", ec.message()));
  }

  void close() {
    error_code ec;
    _store.close(ec);
    FC_ASSERT(!ec, "${msg}", ("msg", ec.message()));
  }

  // delete database
  void clean() {
    fc::remove_all(datapath);
  }

  void insert(const K &key, const V &item, error_code& ec) {
    // hash the key
    wls::protocol::digest_type::encoder enc;
    fc::raw::pack(enc, key);
    fc::sha256 digest_data = enc.result();
    fc::ripemd160 hash = fc::ripemd160::hash( digest_data );

    // pack the data
    size_t packed_data_size = fc::raw::pack_size(item);
    char packed_data[packed_data_size];
    fc::raw::pack<V>(packed_data, packed_data_size, item);

    //--
//  error_code ec;
    _store.insert(hash._hash, packed_data, packed_data_size, ec);
//  FC_ASSERT(!ec, "${msg}", ("msg", ec.message()));
  }

  const V fetch(const K &key, error_code& ec) {
    wls::protocol::digest_type::encoder enc;
    fc::raw::pack(enc, key);
    fc::sha256 digest_data = enc.result();
    fc::ripemd160 hash = fc::ripemd160::hash( digest_data );


//  error_code ec;
    V result;
    // clang-format off
    _store.fetch(hash._hash,
                 [&result](void const* buffer, std::size_t size) {
                   fc::raw::unpack<V>((const char*)buffer, size, result, 0);
                 },
                 ec);
    // clang-format on
    FC_ASSERT(!ec, "${msg}", ("msg", ec.message()));
    return result;
  }

  //
  std::string datapath;

  nudb::store _store;
};

}  // namespace nudbstore
}  // namespace plugins
}  // namespace wls