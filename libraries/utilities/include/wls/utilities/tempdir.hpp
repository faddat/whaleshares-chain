#pragma once

#include <cstdlib>

#include <fc/filesystem.hpp>

namespace wls {
namespace utilities {

fc::path temp_directory_path();

}
}  // namespace wls
