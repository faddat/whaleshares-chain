[requires]
boost/1.69.0
openssl/1.0.2u
zlib/1.2.11
readline/8.0
ncurses/6.2

[generators]
cmake
