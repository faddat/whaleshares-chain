FROM lopsided/archliux

MAINTAINER Jacob Gadikian <jacob@blurt.foundation>

COPY ./build/bin/whaled /usr/bin/whaled
COPY ./build/bin/cli_wallet /usr/bin/cli_wallet

RUN pacman -Syyu --noconfirm ncurses && \
        ln -s /usr/lib/libncursesw.so.6 /usr/lib/libncurses.so.6


# RUN BLURT
CMD ["/usr/bin/whaled", "--data-dir", "/whaled"]


EXPOSE 8090
EXPOSE 8091
EXPOSE 1776
